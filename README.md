# Hello World

This is a guide how to run **Hello World** on kubernetes or docker-compose

## Building and running docker-compose from scratch

Code below will allow you to build application and run locally.
~~~~
docker-compose build ./docker-compose-build.yml
docker-compose up
~~~~

## Running docker-compose from latest stable release

~~~~
docker-compose up
~~~~

## Running on local kubernetes cluster

You have helm installed: https://github.com/helm/helm

You have minikube installed: https://kubernetes.io/docs/tasks/tools/install-minikube/

You started minikube:
~~~~
minikube start
~~~~
Start cluster using helm. Replace namespace with this one which you want to use:
~~~~
helm install --name namespace kube-0.1.0.tgz
~~~~

## Source code for hello and world

Hello application source code is available under: https://gitlab.com/istredd/hello-dotnet/

World application source code is under: https://gitlab.com/istredd/world-dotnet/

## Running cluster depending on your needs

You can run k8s cluster adjusting some of variables to your needs. There is couple of them already available

All chart templates are under ./kube folder available in this repository. Simply modify ./values.yaml file to change default values to whatever you need. You can run your cluser using NodePort or LoadBalancer only at the moment, choose number of replicas, change image repo, and few other things.